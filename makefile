all: compile1 compile2 run1 run2

compile1:
	gcc pi_con_y_sin_threads.c -o pi1 -l pthread -lm

compile2:
	gcc -o mult_matrix_vector mult_matrix_vector.c -l pthread -lm
run1:
	./pi1
run2:
	./mult_matrix_vector
clean:
	find . -type f -name '*.exe' -delete
