//Tarea 5
//Heillen Sosa. B26567
//multiplicacion de matrices

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>
#include<math.h>
#include <pthread.h>
#include<unistd.h>
#include<assert.h>
#include<string.h>


#define NUM_THREADS 8
pthread_mutex_t mutex;

struct serial_params {
  int *vector;
  int **matrix;
  int *vector_result;
};

struct parallel_params {
  int *vector;
  int **matrix;
  int *vector_result;
  int random_size;
};

typedef enum { FALSE, TRUE } boolean;

struct serial_params serial_params;
struct parallel_params parallel_params;
pthread_mutex_t mutexloop;

void mult_vector_matrix_serial (int vector_matrix_capacity){
  //multiplication of vector and square matrix
  int result = 0;
  for (int i = 0; i < vector_matrix_capacity; i++){
    //printf("iteration i: %d\n", i);
    int prev_result = 0;
    for (int j = 0; j < vector_matrix_capacity; j++){
      //printf("iteration j: %d\n", j);
      result = (serial_params.vector[j])*(serial_params.matrix[i][j]);
      prev_result = prev_result + result;
      //printf("%d\n", result);
    }
    //printf("Addition Result: %d\n", prev_result);
    serial_params.vector_result[i] = prev_result;
    //printf("Checking Result: %d\n", serial_params.vector_result[i]);
  }
}


void *mult_vector_matrix_parallel (void *arg){
  long param;
	param = (long)arg;
  pthread_mutex_lock (&mutexloop);
  for (int i = 0; i < parallel_params.random_size; i++){
    parallel_params.vector_result[param] = parallel_params.vector_result[param] + (parallel_params.vector[i])*(parallel_params.matrix[param][i]);
  }
	pthread_mutex_unlock (&mutexloop);
	pthread_exit((void *)0);
}

void print_vectors_to_multiply (int vector_matrix_capacity) {
  //print vector with random values generated
  printf("\n");
  printf("Your serial vector is: ");
  for (int i = 0; i < vector_matrix_capacity; i++){
    printf("%d ", serial_params.vector[i]);
  }
  printf("\n \n");
  printf("Your parallel vector is: ");
  for (int i = 0; i < vector_matrix_capacity; i++){
    printf("%d ", parallel_params.vector[i]);
  }
  printf("\n \n");
}

void print_matrixes_to_multiply (int vector_matrix_capacity) {
  //print square matrix with random values generated
  printf("Your serial matrix is: \n");
  for (int i = 0; i < vector_matrix_capacity; i++){
    for (int j = 0; j < vector_matrix_capacity; j++) {
      printf("%d ", serial_params.matrix[i][j]);
    }
    printf("\n");
  }
  printf("\n");
  printf("Your parallel matrix is: \n");
  for (int i = 0; i < vector_matrix_capacity; i++){
    for (int j = 0; j < vector_matrix_capacity; j++) {
      printf("%d ", parallel_params.matrix[i][j]);
    }
    printf("\n");
  }
  printf("\n");
}

void print_result_vectors(int vector_matrix_capacity){
  //print vector result from earlier multiplication
  printf("The serial result vector is: ");
  for (int i = 0; i < vector_matrix_capacity; i++){
    printf("%d ", serial_params.vector_result[i]);
  }
  printf("\n \n");
  printf("The parallel result vector is: ");
  for (int i = 0; i < vector_matrix_capacity; i++){
    printf("%d ", parallel_params.vector_result[i]);
  }
  printf("\n \n");
}

void checking_vectors_result(int vector_matrix_capacity){
  boolean test_pass_fail = TRUE;
  for (int i = 0; i < vector_matrix_capacity; i++){
    if( (serial_params.vector_result[i]) != (parallel_params.vector_result[i]) ){
      test_pass_fail = FALSE;
      printf("iteration %d\n", i);
    }
  }
  if (test_pass_fail == FALSE) {
    printf("Test fail: Both result vectors aren't equal\n");
  }
  else {
    printf("Test pass: Both result vectors are equal\n");
  }

}


int main() {
  void *status;
	pthread_mutex_init(&mutexloop, NULL);
  pthread_attr_t attr1;
	pthread_attr_init(&attr1);
	pthread_attr_setdetachstate(&attr1, PTHREAD_CREATE_JOINABLE);
  int range, vector_matrix_capacity;
  int upper = 8, lower = 3;
  range = ((upper - lower) + 1);
  srand(time(0));       //Use current time as seed for random generator
  int num = (rand() % range);
  //printf("The random number generated is: %d\n", num);
  num = num + 3;
  //printf("The random number fixed is: %d\n", num);
  vector_matrix_capacity = num;
  //vector_matrix_capacity = 3;//For testing purposes
  printf("vector and matrix length is: %d\n", vector_matrix_capacity);
  parallel_params.random_size = vector_matrix_capacity;

  //Asigning length to both, serial and paralel vectors to multiply
  serial_params.vector = (int *) malloc (vector_matrix_capacity*sizeof(int));
  parallel_params.vector = (int *) malloc (vector_matrix_capacity*sizeof(int));

  //generating vectors with random values in it
  for (int i = 0; i < vector_matrix_capacity; i++){
    int item = rand() % 1000; //using 1k as top number to be able to handle multiplication
    //printf("item :%d\n", item );
    serial_params.vector[i] = item;
    parallel_params.vector[i] = item;
  }

  //Asigning length to both, serial and paralel matrixes to multiply
  serial_params.matrix = (int **) malloc (vector_matrix_capacity*sizeof(int *));
  parallel_params.matrix = (int **) malloc (vector_matrix_capacity*sizeof(int *));
  for (int i = 0; i < vector_matrix_capacity; i++) {
    serial_params.matrix[i] = (int *) malloc(vector_matrix_capacity*sizeof(int));
    parallel_params.matrix[i] = (int *) malloc(vector_matrix_capacity*sizeof(int));
  }

  //generating square matrixes with random values in it
  for (int i = 0; i < vector_matrix_capacity; i++){
    for (int j = 0; j < vector_matrix_capacity; j++) {
      int items = rand() % 1000; //using 1k as top number to be able to handle multiplication
      //printf("item :%d\n", items );
      serial_params.matrix[i][j] = items;
      parallel_params.matrix[i][j] = items;
    }
  }

  print_vectors_to_multiply(vector_matrix_capacity);
  print_matrixes_to_multiply (vector_matrix_capacity);

  //Asigning length to both, serial and paralel result vectors
  serial_params.vector_result = (int *) malloc (vector_matrix_capacity*sizeof(int));
  parallel_params.vector_result = (int *) malloc (vector_matrix_capacity*sizeof(int));

  //filling with zeros both, serial and parallel result vectors
  for (int i = 0; i < vector_matrix_capacity; i++){
    serial_params.vector_result[i] = 0;
    parallel_params.vector_result[i] = 0;
  }

  mult_vector_matrix_serial (vector_matrix_capacity);

  // Array of threads
	pthread_t threads_m[vector_matrix_capacity];
	// threads rutine
	long fi = 0;
	int thr;
	for(int h = 0; h < vector_matrix_capacity; h++){
		fi = h;
		thr = pthread_create(&threads_m[h], NULL, (void *)mult_vector_matrix_parallel, (void *)fi);
		if(thr)
		{
			printf("ERROR; return code from pthread_create() is %d\n", thr);
			exit(-1);
		}
	}
	pthread_attr_destroy(&attr1);
	// Joining threads
	for(int i = 0; i < vector_matrix_capacity; i++){
		pthread_join(threads_m[i], &status);
  }
  printf("Resultados multiplicacion de matriz y vector \n");
  print_result_vectors(vector_matrix_capacity);
  checking_vectors_result(vector_matrix_capacity);
}
