///Tarea 5
//Maria Jose Arce
//B60561

//Descripcion:
//vamos a utilizar el metodo de montecarlo para relizar la aproximacion de pi
//para realizar la aproximacion con este metodo 
//debemos seguir los siguientes pasos:
//tenemos que considerear un circulo unitario de radio = 1.
//dentro de un cuadrado con los lados = 2. 
//Ahora si escogemos un punto al azar (x,y) donde x y y estan entre -1,1, la probabilidad de que este punto al azar se encuentre dentro del circuulo unitario se da 
//como la proporcion  entre el area el circulo unitario y el cuadrado
//P(x^2+y^2<1) = A_circulo/A_cuadrado = Pi/4
//Si escogemos puntos al azar N veces y M veces de esas el punto cae dentro del circulo unitario, 
//la probabilidad de que un punto al azar caiga dentro de dicho circulo estara dado por:
//Po(x^2+y^2<1)=M/N
//donde el subindice indica que es una distribucion discreta (por ser M y N numeros enteros).
//Pero si N es un numero muy grande (infinito) las 2 probabilidades deberan ser iguales y podemos escribir:
//PI = 4M/N

//includes
#include<stdio.h>
#include<pthread.h>
#include<math.h>
#include<stdlib.h>
#include<unistd.h>
#include<assert.h>
#include<time.h>
#include<string.h>


//definimos un rango de para escoger puntos al azar
#define rango 2000000.0 //entre mas grande mayor precision y mayor tiempo dura mi compu en procesarlo
#define num_hilos 8

//creamos el semaforo, este garantizan la mutua exclusion de una region critica.
pthread_mutex_t mutex;


//variable global
float pi = 0.0;

//funciones
float ran();
float pii();
void *pii_paralelo(void *argumento);
int main(void);
//funcion para calcular un valor random
//Donde RAND_MAX es una expresion constante de intervalo, el valor del cual es el valor max retornado por la funcion rand.
//El valor de la macro RAND)MAX sera de al menos 32767.
float ran(){
   float r;
   r = rand()/(float)RAND_MAX;
   return r;
}

//metodo 1 para encontrar la aproximacion de pi sin paralelismo
float pii(){
    //iniciamos variables internas
    float NN = 0.0;
    float MM = 0.0;
    float n = 0.0;
    float m = 0.0;
    //hacemos bucle para rango definido
    for (int i = 0; i < rango*num_hilos; i ++){
        n = ran(); 
        m = ran();
        float result = n*n + m*m;
        float raiz = sqrt(result); //esto para poder verificarlo con la unidad
        if(raiz < 1){//revisamos P(x^2+y^2<1) = A_circulo/A_cuadrado = Pi/4
            MM ++; 
        }
        NN++;
    }
    float result2 = (float)MM / (float)NN;
    float pi = 4.0 * result2;
    printf("Pi sin paralelismo: %f\n ", pi);
    return pi;
}

//metodo 2 se trata del mismo metodo 1 solo que aca aplicamos el paralelismo y se aplican semaforos mutex
void *pii_paralelo(void *argumento){
    //iniciamos variables internas
    float NN = 0.0;
    float MM = 0.0;
    float n = 0.0;
    float m = 0.0;
    //trabajamos variable que estamos ingresando como parametro
    float *arg = (float*)argumento;
    float num = *arg; //esta variable nos dice en que hilo estamos
    //
     for (int i = 0; i < rango*num_hilos; i ++){
        //aca podrian haber errores ya que rand podria dar resultados diferentes en procesos simultaneos
        n = ran();
        m = ran();
        float result = n*n + m*m; 
        float raiz = sqrt(result);//esto para poder verificarlo con la unidad
        if(raiz < 1){//revisamos P(x^2+y^2<1) = A_circulo/A_cuadrado = Pi/4
            MM ++;
        }
        NN++;
    }
    //pthread_mutex_lock(&mutex);
    float result2 = (float)MM / (float)NN;
    pthread_mutex_lock(&mutex);
    pi = result2; //guardamos resultado2 en la variable global
    pi = 4.0*result2;
    pthread_mutex_unlock(&mutex);
    //printf("Pi por cada hilo: %f\n ", pi);
    //no retorna nada
}

   
//funcion principal
int main(void){
    //variables para contar el tiempo
    double tiempo_i1;
    double tiempo_f1;
    double tiempo_i2;
    double tiempo_f2;
    float tiempo_total_segundos1;
    float tiempo_total_segundos2;
    int retorno =0;
    int tnum[num_hilos];//parametro que pasamos a pii_paralelo
    //empezamos a contar el tiempo de la primera funcion
    tiempo_i1 = clock();
    printf("------------------------------ \n");
    printf("Pi aproximado sin paralelismo \n");
    printf("------------------------------ \n");
    float pi1 = pii();
    tiempo_f1 = clock();
    tiempo_total_segundos1 = (tiempo_f1-tiempo_i1)/CLOCKS_PER_SEC;
    printf("El tiempo total de ejecucion es: %f \n",tiempo_total_segundos1);
    printf("------------------------------ \n");
    printf("Pi aproximado con paralelismo \n");
    printf("------------------------------ \n");
    pthread_t hilos[num_hilos];
    int i;//contador
    int result;//variable para la validacion de hilos
    tiempo_i2 = clock();
    //inicializamos semaforos
    int ini = pthread_mutex_init(&mutex,NULL);
    //validacion del mutex
    if(ini!= 0 ){
        printf("Error mutex");
        retorno =1;
    }
    //creamos los 8 hilos
    for(i=0;i<num_hilos;i++){
        tnum[i] = i;
        result = pthread_create(&hilos[i],NULL,(void *)pii_paralelo,&tnum[i]);
        assert(!result);
    }
    //juntamos los hilos y se hace validacion del join
    for(i=0;i<num_hilos;i++){
        result = pthread_join(hilos[i],NULL);
        assert(!result);
    }
    float pi2 = pi;//se llama variable global y se multiplica por 4 para encontrar la aproximacion
    printf("Pi total con paralelismo:%f \n",pi2);
    tiempo_f2 = clock();
    tiempo_total_segundos2 = (tiempo_f2-tiempo_i2)/CLOCKS_PER_SEC;
    printf("El tiempo total de ejecucion es: %f \n",tiempo_total_segundos2);
    float diferencia = pi2-pi1;
    printf("La diferencia entre ambos PI's es:%f \n",diferencia);
    pthread_mutex_destroy(&mutex);//destruimos mutex creado
    //para testear
    int v1 = (int)(pi1*100);
    int v2 = (int)(pi2*100);
    double v11 = (double)v1/100;
    double v22 = (double)v2/100;
    printf("resultados PI \n");

    if(v11!=v22){
        printf("resultados incorrectos \n");
        
        retorno = 1;//uno va a significar que hay un error
    }
    else
    {
        printf("resultados correctos \n");
        retorno = 0;
    }
    return retorno;//cero no hay errores
}
